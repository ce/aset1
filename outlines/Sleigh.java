/*package com.clemensehrenreich;
*/
/**
 * Created by clemens on 11/01/16.
 */
/*
public class Sleigh {
    private int l_currentPresents = 0;
    private int r_currentPresents = 0;
    private Present[] presents;
    public int maxPresents = 120;

    public Sleigh () {
        this.presents = new Present[maxPresents];
    }
    */
    public synchronized int getCurrentPresents () { return (this.l_currentPresents + this.r_currentPresents); }
    public synchronized boolean spaceLeft() { return (this.l_currentPresents + this.r_currentPresents) < this.maxPresents;}
    public synchronized boolean addPresent(Present toAdd) {
        /*
         The presents are adding and sorted at the same time.
         Female presents start at 0 and move upwards, while male presents start at maxPresents and move downwards.
         */
        if (this.spaceLeft()) {
            if (toAdd.GetGender().equals("Female")) {
                this.presents[this.l_currentPresents] = toAdd;
                this.l_currentPresents += 1;
            } else {
                this.presents[this.maxPresents - 1 - this.r_currentPresents] = toAdd;
                this.r_currentPresents += 1;
            }
            return true;
        }
        else {
            return false;
        }
    }
    public synchronized int getMaxAvailablePresents() {
        if (this.getCurrentPresents() >= 6) return Math.min(this.getCurrentPresents(), 10);
        else return 0;
    }
    public synchronized Present[] removePresents(int number, String Gender) {
        Present[] returnValues = new Present[number];
        int returnIndex = 0;

        // If there are not enough presents for the request gender, return null.
        if (Gender != null && Gender.equals("Female") && this.l_currentPresents == 0) return null;
        else if (Gender != null && Gender.equals("Male") && this.r_currentPresents == 0) return null;

        if (this.getCurrentPresents() >= number && number > 0) {
            for (int i = 0; i < number; i++) {
                /*
                 Presents are removed alternating from the beginning and the end of the array.
                 With the sorting at the input, this ensures a balanced output of presents.
                 */

                if ((this.l_currentPresents > 0)  && ((i % 2 == 0) || this.r_currentPresents == 0)) {
                    returnValues[returnIndex] = this.presents[0];
                    returnIndex++;
                    for (int iL = 0; iL < this.l_currentPresents; iL++) {
                        this.presents[iL] = this.presents[iL+1];
                    }
                    this.l_currentPresents -= 1;
                }
                else if ((this.r_currentPresents > 0) && ((i % 2 != 0) || this.l_currentPresents == 0)) {
                    returnValues[returnIndex] = this.presents[this.maxPresents-1];
                    returnIndex++;

                    for (int iR = maxPresents-1; iR < this.r_currentPresents; iR--) {
                        this.presents[iR] = this.presents[iR-1];
                    }
                    this.r_currentPresents -= 1;
                }
            }
            return returnValues;
        }
        else return null;
    }
}
