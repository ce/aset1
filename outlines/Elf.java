/*package com.clemensehrenreich;

import java.text.SimpleDateFormat;
import java.util.*;
*/
/**
 * Created by clemens on 10/01/16.
 */
public class Elf extends TaskQueue {
    /*
    private int NumberOfPresents = 0;
    private int NumberOfPresentsPutOnSleigh = 0;
    private int WaitingTime = 0;
    private List<Present> Presents;
    private Sleigh sleigh;
    private ResponsibleElf responsibleElf;

    static int GIFT_SELECTION_MIN = 1;
    static int GIFT_SELECTION_MAX = 4;
    static int WRAPPING_MIN = 1;
    static int WRAPPING_MAX = 4;
    static int MAX_PRESENTS = 2;

    private int numberOfPresentsCurrentlyHolding = 0;

    public Elf(Sleigh sleigh, ResponsibleElf responsibleElf, String Name) {
        super();
        */
        synchronized (this) {
            /*
            this.sleigh = sleigh;
            this.Name = Name;
            this.TaskQueue = new Task[0];
            this.responsibleElf = responsibleElf;
            this.DEBUG = Main.DEBUG;
            this.Presents = new ArrayList<>();

            this.fileName = this.getClass().getSimpleName() + this.GetName() + new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss.SSS").format(new Date()) + ".log";
            */
            Runnable checkForResponsibleElf = () -> {
                if (this.responsibleElf.isBlowingTheHorn()) {
                    this.log(" is dancing to the horn", this.DEBUG);
                    this.TaskQueue[this.CurrentTaskIndex].waitDuration(1);
                }
            };

            Task picking = new Task(GIFT_SELECTION_MIN, GIFT_SELECTION_MAX, "picking", checkForResponsibleElf, () -> {
                if (this.GetNumberOfPresentsCurrentlyHolding() < MAX_PRESENTS) {
                    this.numberOfPresentsCurrentlyHolding += 1;
                    this.CurrentTaskIndex -= 1;
                }
                this.log("done picking", this.DEBUG);
            });
            this.addTaskToQueue(picking);

            Task present = new Task(0, "present", null, () -> {
                synchronized (this) {
                    if(this.Presents.size() < MAX_PRESENTS) {
                        this.Presents.add(new Present());
                        this.CurrentTaskIndex -= 1;
                    }
                    this.log("got a new present", this.DEBUG);
                }
            });
            this.addTaskToQueue(present);

            Task wrapping = new Task(WRAPPING_MIN, WRAPPING_MAX, "wrapping", checkForResponsibleElf, () -> {
                synchronized (this) {
                    if (this.GetNumberOfPresentsCurrentlyHoldingAndWrapped() < MAX_PRESENTS) {
                        // wrap the first unwrapped present
                        this.Presents.stream().filter(p -> !p.GetWrappedStatus()).findFirst().get().SetWrappedStatus(true);
                        this.CurrentTaskIndex -= 1;
                        this.NumberOfPresents += 1;
                        this.log("done wrapping", this.DEBUG);
                    }
                }
            });
            this.addTaskToQueue(wrapping);

            Task bringToSleigh = new Task(0, "delivering", checkForResponsibleElf, () -> {
                synchronized (this) {
                    // deliver all the presents to the sleigh
                    Iterator<Present> presentIterator = this.Presents.iterator();
                    while (presentIterator.hasNext()) {
                        Present p = presentIterator.next();
                        if (this.sleigh.addPresent(p)) {
                            this.NumberOfPresentsPutOnSleigh += 1;
                            presentIterator.remove();
                            this.log("added present to sleigh", this.DEBUG);
                        } else {
                            this.log("failed to add to sleigh, waiting", this.DEBUG);
                            this.WaitingTime += 1;
                            this.CurrentTaskIndex -= 1;
                            break;
                        }
                    };
                }
            });
            this.addTaskToQueue(bringToSleigh);
        }
    }
    private synchronized int GetNumberOfPresentsCurrentlyHolding () { return this.numberOfPresentsCurrentlyHolding; }
    private synchronized long GetNumberOfPresentsCurrentlyHoldingAndWrapped() {
        return this.Presents.stream().filter(Present::GetWrappedStatus).count();
    }
    public synchronized int GetNumberOfPresents() { return this.NumberOfPresents; }
    public synchronized int GetNumberOfPresentsPutOnSleigh() { return this.NumberOfPresentsPutOnSleigh; }
    public synchronized int GetWaitingTime() { return this.WaitingTime; }
    public synchronized void Report() {
        this.log("Wrapped " + this.GetNumberOfPresents() + " and waited: " + this.GetWaitingTime(), true);
    }

}
