/*package com.clemensehrenreich;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
*/
/**
 * Created by clemens on 11/01/16.
 */
public class Santa extends TaskQueue {
    /*
    private int TimeSpentWaiting = 0;
    private int PresentsDistributed = 0;

    static int CHILD_SELECTION_MIN = 1;
    static int CHILD_SELECTION_MAX = 4;

    private int SackSize = 10;
    private int CurrentSizeOfSack = 0;
    private ArrayList<Present> Sack;

    private Sleigh sleigh;
    private String Department;
    private String currentChildGender;

    private static Map<String, Integer> Departments;
    static {
        Departments = new HashMap<>();
        Departments.put("Clothes", 3);
        Departments.put("Toys", 5);
        Departments.put("Outside Gear", 2);
        Departments.put("Video Games", 4);
    }

    public Santa(Sleigh sleigh, String Name) {
        super();
        */
        synchronized (this) {
            /*
            this.Sack = new ArrayList<>(SackSize);
            this.sleigh = sleigh;
            this.Name = Name;
            this.TaskQueue = new Task[0];
            this.DEBUG = Main.DEBUG;
            this.currentChildGender = null;

            this.fileName = this.getClass().getSimpleName() + this.GetName() + new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss.SSS").format(new Date()) + ".log";
            
            // pick a random department
            this.Department = Departments.keySet().stream().collect(Collectors.toList()).get(ThreadLocalRandom.current().nextInt(0, Departments.size()));
            this.log(" has " + this.Department, this.DEBUG);
            */
            Task walkingToDepartment = new Task(Departments.get(this.Department), "walkingToDepartment", null, () -> this.log("walked to department", this.DEBUG));
            this.addTaskToQueue(walkingToDepartment);

            Task fillUpSack = new Task(0, 1, "fillingUpSack", null, () -> {
                synchronized (this) {
                    this.log("max available presents: " + Integer.toString(this.sleigh.getMaxAvailablePresents()), this.DEBUG);

                    // Put all the presents back on the sleigh, to ensure a balanced mix of presents.
                    this.Sack.stream().forEach(present -> this.sleigh.addPresent(present));
                    this.Sack.clear();

                    Present[] presentsToReceive;
                    if (this.sleigh.getCurrentPresents() >= 6 && this.Sack.size() <= 10) {

                        presentsToReceive = this.sleigh.removePresents(Math.min(10-this.Sack.size(), this.sleigh.getCurrentPresents()), this.currentChildGender);

                        if (presentsToReceive != null) {
                            this.Sack.addAll(this.Sack.size(), Arrays.asList(presentsToReceive));
                            this.log("now has " + this.Sack.size() + " presents", this.DEBUG);
                        }
                        else {
                            this.log("is waiting for presents", this.DEBUG);
                            this.TimeSpentWaiting += 1;
                            this.CurrentTaskIndex -= 1;
                        }
                    }
                    else {
                        this.log("is waiting for presents", this.DEBUG);
                        this.TimeSpentWaiting += 1;
                        this.CurrentTaskIndex -= 1;
                    }
                }
            });
            this.addTaskToQueue(fillUpSack);

            Task walkingBack = new Task(Departments.get(this.Department), "walkingToDepartment", null, () -> this.log("walked back", this.DEBUG));
            this.addTaskToQueue(walkingBack);

            Task givingOutPresents = new Task(CHILD_SELECTION_MIN, CHILD_SELECTION_MAX, "givingOutPresents", null, () -> {
                synchronized (this) {

                    if (this.Sack.size() > 0 && this.Sack.size() <= 10) {
                        // Pick a random gender
                        String childGender = ThreadLocalRandom.current().nextBoolean() ? "Male" : "Female";
                        if (this.GetNumOfPresentsWithGender(childGender) == 0) {
                            this.currentChildGender = childGender;
                            this.log("needs to refill", this.DEBUG);
                        } else {
                            this.log("Giving out to " + childGender + " customer: " + this.GetToy(childGender).GetType(), this.DEBUG);
                            this.PresentsDistributed += 1;
                            this.CurrentTaskIndex -= 1;
                            this.currentChildGender = null;
                        }
                    }
                }
            });
            this.addTaskToQueue(givingOutPresents);
        }
    }
    private synchronized void CountSack() { this.CurrentSizeOfSack = this.Sack.size(); }
    private synchronized long GetNumOfPresentsWithGender(String gender) {
        return this.Sack.stream().filter(element -> element.GetGender().equals(gender)).count();
    }
    private synchronized Present GetToy(String Gender) {
        Present retVal = this.Sack.stream().filter(element -> element.GetGender().equals(Gender)).findFirst().get();
        this.Sack.remove(retVal);
        return retVal;
    }
    public synchronized void Report() {
        this.log("Delivered " + this.GetNumberOfDistributedPresents() + " and waited: " + this.GetWaitingTime(), true);
    }
    public synchronized int GetNumberOfDistributedPresents() { return this.PresentsDistributed; }
    public synchronized int GetWaitingTime() { return this.TimeSpentWaiting; }
    public synchronized int GetSackSize() { this.CountSack(); return this.CurrentSizeOfSack; }

}
