/*package com.clemensehrenreich;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.IntStream;

public class Main {

    public static boolean DEBUG = true;

    public static void main(String[] args) {
        */
        ExecutorService exec = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        /*Sleigh masterSleigh = new Sleigh();

        ResponsibleElf theLeader = new ResponsibleElf(masterSleigh);

        List<Elf> elfs = new ArrayList<>();

        elfs.add(new Elf(masterSleigh, theLeader, "Clemens"));
        elfs.add(new Elf(masterSleigh, theLeader, "Johann"));
        elfs.add(new Elf(masterSleigh, theLeader, "Fritz"));
        elfs.add(new Elf(masterSleigh, theLeader, "Franky"));
        elfs.add(new Elf(masterSleigh, theLeader, "Adi"));
        elfs.add(new Elf(masterSleigh, theLeader, "Dude"));

        List<Santa> santas = new ArrayList<>();

        santas.add(new Santa(masterSleigh, "A"));
        santas.add(new Santa(masterSleigh, "B"));
        santas.add(new Santa(masterSleigh, "C"));

        // i is equal to the simulation time
        IntStream.range(1, 4801).forEach(i -> {
            */
            Collection<Future<?>> tasks = new LinkedList<Future<?>>();

            //submit the work for this round
            elfs.stream().forEach(elf -> tasks.add(exec.submit(elf.getWork(i))));
            santas.stream().forEach(santa -> tasks.add(exec.submit(santa.getWork(i))));
            exec.submit(theLeader.getWork(i));

            // retrieve the results, joining up the threads
            tasks.stream().forEach(task -> {
                try {
                    task.get();
                }
                catch (InterruptedException interrupt) {
                    interrupt.printStackTrace();
                    System.out.println("Got interrupted:" + interrupt.getMessage());
                }
                catch (ExecutionException exeExc) {
                    exeExc.printStackTrace();
                    System.out.println("There was a problem: " + exeExc.getMessage());
                }
            });
            /*
            if ((i % 60) == 0) {
                // Reporting Time
                elfs.stream().forEach(Elf::Report);
                santas.stream().forEach(Santa::Report);
            }

        });

        if (elfs.stream().mapToInt(Elf::GetNumberOfPresentsPutOnSleigh).sum() ==
                (santas.stream().mapToInt(Santa::GetNumberOfDistributedPresents).sum())
                + (santas.stream().mapToInt(Santa::GetSackSize).sum())
                + masterSleigh.getCurrentPresents())
                    System.out.println("Everything is correct.");
        */
        exec.shutdown();
    }
}
