package com.clemensehrenreich;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by clemens on 10/01/16.
 */
public class Task {
    private int Duration;
    private int DurationLeft;
    private String Name;
    private int LowerBound;
    private int UpperBound;
    private Runnable beforeCheckCallback;
    private Runnable afterDoneCallback;
    private boolean staticTime;

    private void initialize(String name,  Runnable beforeCheckCallback, Runnable afterDoneCallback) {
        this.Name = name;
        this.beforeCheckCallback = beforeCheckCallback;
        this.afterDoneCallback = afterDoneCallback;
    }

    private void initializeRandom() {
        this.Duration = this.DurationLeft =  ThreadLocalRandom.current().nextInt(this.LowerBound, this.UpperBound);
        this.staticTime = false;
    }

    public Task(int LowerBound, int UpperBound, String Name, Runnable beforeCheckCallback, Runnable afterDoneCallback) {
        this.LowerBound = LowerBound;
        this.UpperBound = UpperBound;
        this.initialize(Name, beforeCheckCallback, afterDoneCallback);
        this.initializeRandom();
    }
    public Task(int duration, String Name, Runnable beforeCheckCallback, Runnable afterDoneCallback) {
        this.Duration = this.DurationLeft = duration;
        this.staticTime = true;
        this.initialize(Name, beforeCheckCallback, afterDoneCallback);
    }
    public boolean ProcessTick(int duration) {
        if (this.beforeCheckCallback != null) {
            this.beforeCheckCallback.run();
        }
        if (this.DurationLeft > 0) {
            this.DurationLeft -= duration;
            return true;
        }
        else {
            try {
                this.afterDoneCallback.run();
                if (this.staticTime) this.DurationLeft = this.Duration;
                else this.initializeRandom();
                return false;
            }
            catch (Exception exception) {
                exception.printStackTrace();
            }
        }
        return false;
    }
    public void waitDuration (int duration) {
        this.DurationLeft += duration;
    }

}
