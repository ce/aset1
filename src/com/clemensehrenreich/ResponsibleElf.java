package com.clemensehrenreich;

/**
 * Created by clemens on 11/01/16.
 */
public class ResponsibleElf extends TaskQueue {

    private int BLOW_DURATION_MIN = 3;
    private int BLOW_DURATION_MAX = 6;

    private boolean isBlowingTheHorn;
    private Sleigh theSleigh;

    public synchronized boolean isBlowingTheHorn() {
        return this.isBlowingTheHorn;
    }

    public ResponsibleElf(Sleigh theSleigh) {
        super();
        synchronized (this) {
            this.isBlowingTheHorn = false;
            this.TaskQueue = new Task[0];
            this.theSleigh = theSleigh;
            this.DEBUG = Main.DEBUG;

            Task waitToBlowTheHorn = new Task(0, "waitToBlowTheHorn", null, () -> {
                if (this.theSleigh.getCurrentPresents() > ((long)this.theSleigh.maxPresents * 0.75)) {
                    this.log("is blowing the horn! Also changed the sign", this.DEBUG);
                    this.isBlowingTheHorn = true;
                }
            });
            this.addTaskToQueue(waitToBlowTheHorn);

            Task blowTheHorn = new Task(BLOW_DURATION_MIN, BLOW_DURATION_MAX, "blowTheHorn", null, () -> this.isBlowingTheHorn = false);
            this.addTaskToQueue(blowTheHorn);
        }
    }
}
