package com.clemensehrenreich;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by clemens on 10/01/16.
 */
public class Present {
    private enum PresentType {
        Train, Doll, Dinosaur, Whistle, FakeTattoo, Bracelet
    }
    private PresentType Type;
    private String Gender;
    private String PresentColour;
    private boolean Wrapped;

    public Present() {
        synchronized (this) {
            // Pick a gender
            this.Gender = ThreadLocalRandom.current().nextBoolean() ? "Male" : "Female";

            // Pick a present
            this.Type = PresentType.values()[ThreadLocalRandom.current().nextInt(0, PresentType.values().length)];

            this.SetWrappedStatus(false);
        }
    }
    public synchronized String GetType() {
        return this.Type.name();
    }
    public synchronized String GetGender() {
        return this.Gender;
    }
    public synchronized void SetWrappedStatus(boolean Status) {
        this.PresentColour = (this.GetGender().equals("Female")) ?
                (ThreadLocalRandom.current().nextBoolean() ? "Pink" : "Silver") :
                (ThreadLocalRandom.current().nextBoolean() ? "Red" : "Blue");

        this.Wrapped = Status;
    }
    public synchronized boolean GetWrappedStatus() { return this.Wrapped; }
}
