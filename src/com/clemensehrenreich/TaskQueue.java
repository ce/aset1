package com.clemensehrenreich;

import java.io.FileOutputStream;
import java.util.Arrays;

/**
 * Created by clemens on 11/01/16.
 */
public class TaskQueue {
    protected Task[] TaskQueue;
    protected int CurrentTaskIndex = 0;
    protected int time = 0;

    protected String Name;

    protected FileOutputStream fileLog;
    protected String fileName;

    protected boolean DEBUG = true;

    protected synchronized void addTaskToQueue(Task toAdd) {
        this.TaskQueue = Arrays.copyOf(this.TaskQueue, this.TaskQueue.length + 1);
        this.TaskQueue[TaskQueue.length - 1] = toAdd;
    }
    protected synchronized void processTask(int duration) {
        if (!this.TaskQueue[this.CurrentTaskIndex].ProcessTick(duration)) {
            if (this.CurrentTaskIndex == (this.TaskQueue.length - 1)) this.CurrentTaskIndex = 0;
            else this.CurrentTaskIndex += 1;
        }
    }
    public synchronized Runnable getWork(int time) {
        this.time = time;
        return () -> this.processTask(1);
    }
    public synchronized String GetName() { return this.Name; }

    protected synchronized void log(String toLog, boolean console) {
        StringBuilder log = new StringBuilder();

        log.append("Time: ")
                .append(this.time)
                .append(" ")
                .append(this.getClass().getSimpleName())
                .append(" ")
                .append(this.GetName())
                .append(": ")
                .append(toLog);

        if (console) {
            System.out.println(log.toString());
        }
        else {
            try {
                fileLog = new FileOutputStream(this.fileName, true);
                fileLog.write(String.format(log.toString() + "%n").getBytes());
                fileLog.close();
            } catch (Exception exception) {
                System.out.println(exception.getMessage());
            }
        }
    }
}
